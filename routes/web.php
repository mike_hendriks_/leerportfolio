<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');
Route::get('/course/{slug}', 'PageController@course');
Route::get('/assignment/{slug}', 'PageController@assignment');

// CMS Pages
Auth::routes();

Route::group(['middleware' => 'auth' ], function () {
    // Route::get('/dashboard', 'PageController@cms')->name('dashboard');

    // Courses
    Route::get('/dashboard', 'CourseController@index')->name('course_overview');
    Route::view('/create_course', 'cms.course.create');
    Route::post('/create_course', 'CourseController@create')->name('create_course');
    Route::get('/edit_course/{id}', 'CourseController@read');
    Route::post('/edit_course', 'CourseController@update')->name('update_course');
    Route::get('/delete_course/{id}', 'CourseController@delete');

    // Assignments
    Route::get('/assignments/{id}', 'AssignmentController@index')->name('assignment_overview');
    Route::get('/create_assignment/{course_id}', function ($course_id) {
        return view('cms.assignment.create')->with('course_id', $course_id);
    });
    Route::post('/create_assignment', 'AssignmentController@create')->name('create_assignment');
    Route::get('/edit_assignment/{id}', 'AssignmentController@read');
    Route::post('/edit_assignment', 'AssignmentController@update')->name('update_assignment');
    Route::get('/delete_assignment/{id}', 'AssignmentController@delete');

    // Posts
    Route::get('/posts/{assignment_id}', 'PostController@index')->name('post_overview');
    Route::get('/edit_post/{id}', 'PostController@read');
    Route::post('/edit_post', 'PostController@update')->name('update_post');
    Route::get('/delete_post/{id}', 'PostController@delete');
    Route::view('/create_post/{assignment_id}', 'cms.post.create');
    Route::get('/create_post/{assignment_id}', function ($assignment_id) {
        return view('cms.post.create')->with('assignment_id', $assignment_id);
    });
    Route::post('/create_post', 'PostController@create')->name('create_post');



});
