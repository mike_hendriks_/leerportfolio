<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['course_id', 'title', 'introduction', 'image'];

    protected $table = 'assignments';


    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
