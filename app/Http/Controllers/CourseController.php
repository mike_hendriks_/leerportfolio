<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $courses = Course::all();
        return view('cms.course.overview', ['courses' => $courses]);
    }

    public function create(Request $request)
    {
        $course = new Course;
        $course->abbreviation = $request->abbreviation;
        $course->title = $request->title;
        $course->slug = $request->slug;
        if($request->hasFile('course_image')) {
            $course->img_url = $request->file('course_image')->store('courses', 'images');
        }

        $course->save();

        return redirect(session('links')[1]);
    }

    public function read(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        return view('cms.course.edit', ['course' => $course]);
    }

    public function update(Request $request)
    {
        $course = Course::findOrFail($request->id);
        $course->abbreviation = $request->abbreviation;
        $course->title = $request->title;
        $course->slug = $request->slug;
        if ($request->file('course_image')) {
            $course->img_url = $request->file('course_image')->store('courses', 'images');
        }
        $course->save();

        return redirect(session('links')[1]);
    }

    public function delete(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $course->delete();

        return redirect(session('links')[1]);
    }
}
