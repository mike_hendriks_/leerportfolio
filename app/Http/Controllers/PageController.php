<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Assignment;

class PageController extends Controller
{
    public function home()
    {
        if ($courses = Course::all()) {
            return view('home', ['courses' => $courses]);
        }
    }

    public function course($slug)
    {
        if ($course = Course::where('slug', $slug)->firstOrFail()) {
            return view('course', ['assignments' => $course->assignments, 'course' => $course]);
        }
    }

    public function assignment($slug)
    {
        if ($assignment = Assignment::where('slug', $slug)->firstOrFail()) {
            $previous_assignment = Assignment::find(Assignment::where('course_id', $assignment->course_id)->where('id', '<', $assignment->id)->max('id'));
            $next_assignment = Assignment::find(Assignment::where('course_id', $assignment->course_id)->where('id', '>', $assignment->id)->min('id'));


            // print_r($previous_assignment);
            // exit;
            // if (!empty($previous_assignment)) {
            //     // code...
            // }
            return view('assignment', ['posts' => $assignment->posts, 'assignment' => $assignment, 'previous' => $previous_assignment, 'next' => $next_assignment]);
            // return view('assignment', ['posts' => $assignment->posts, 'assignment' => $assignment]);
        }
    }


    /**
     * Show the application dashboard.
     */
    public function cms()
    {
        return view('cms.home');
    }
}
