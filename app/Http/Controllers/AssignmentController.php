<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Assignment;
use App\Post;

class AssignmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the coursepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($course_id)
    {
        $course = Course::findOrFail($course_id);
        return view('cms.assignment.overview', ['course' => $course, 'assignments' => $course->assignments]);
    }

    //
    // public function posts($course_id)
    // {
    //     // Get course to get the according posts
    //     $course = Course::find($course_id);
    //     $assignment = $course->posts;
    //     return view('cms.post.overview', ['course' => $course, 'posts' => $assignment]);
    // }

    public function create(Request $request)
    {
        $assignment = new Assignment;
        $assignment->course_id = $request->course_id;
        $assignment->title = $request->title;
        $assignment->slug = $request->slug;
        $assignment->introduction = $request->introduction;
        $assignment->image = $request->file('assignment_image')->store('assignments', 'images');

        $assignment->save();

        return redirect(session('links')[2]);
    }

    public function read($id)
    {
        $assignment = Assignment::findOrFail($id);

        return view('cms.assignment.edit', ['assignment' => $assignment]);
    }

    public function update(Request $request)
    {
        $assignment = Assignment::findOrFail($request->id);
        $assignment->title = $request->title;
        $assignment->slug = $request->slug;
        $assignment->introduction = $request->introduction;
        if ($request->file('course_image')) {
            $assignment->image = $request->file('assignment_image')->store('assignments', 'images');
        }

        $assignment->save();

        return redirect(session('links')[2]);
    }

    public function delete(Request $request, $id)
    {
        $assignment = Assignment::findOrFail($id);
        $assignment->delete();

        return redirect(session('links')[2]);
    }
}
