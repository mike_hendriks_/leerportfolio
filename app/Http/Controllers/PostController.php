<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Course;
use App\Assignment;
use App\Post;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the coursepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($assignment_id)
    {
        // $assignments = Assignment::all();
        // return view('cms.post.course_overview', ['assignments' => $assignments]);

        $assignment = Assignment::find($assignment_id);
        $posts = $assignment->posts;
        return view('cms.post.overview', ['assignment' => $assignment, 'posts' => $assignment->posts]);
    }


    // public function posts($course_id)
    // {
    //     // Get course to get the according posts
    //     $course = Assignment::find($course_id);
    //     $posts = $course->posts;
    //     return view('cms.post.overview', ['course' => $course, 'posts' => $posts]);
    // }

    public function create(Request $request)
    {
        $post = new Post;
        $post->assignment_id = $request->assignment_id;
        $post->type = $request->type;
        $post->title = $request->title;
        $post->content = $request->content;
        $images = array();
        if($request->hasFile('post_image')) {
            foreach ($request->file('post_image') as $file) {
                $images[] = $file->store('posts', 'images');
            }
            $post->images = serialize($images);
        }
        if($request->hasFile('post_file')) {
            $post->file = $request->file('post_file')->store('files', 'uploads');
        }

        $post->save();

        return redirect(session('links')[1]);

    }

    public function read($id)
    {
        $post = Post::findOrFail($id);

        return view('cms.post.edit', ['post' => $post]);
    }

    public function update(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $post->type = $request->type;
        $post->title = $request->title;
        $post->content = $request->content;

        $images = array();
        if($request->hasFile('post_image')) {
            foreach ($request->file('post_image') as $file) {
                $images[] = $file->store('posts', 'images');
            }
            $post->images = serialize($images);
        }
        if($request->hasFile('post_file')) {
            $post->file = $request->file('post_file')->store('files', 'uploads');
        }

        $post->save();

        return redirect(session('links')[1]);

    }

    public function delete(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return redirect(session('links')[1]);

    }
}
