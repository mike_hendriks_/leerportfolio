<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['abbreviation', 'title', 'slug', 'image'];

    protected $table = 'courses';

    public function assignments()
    {
        return $this->hasMany('App\Assignment');
    }
}
