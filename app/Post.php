<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['type', 'title', 'content', 'images', 'file'];

    protected $table = 'posts';
}
