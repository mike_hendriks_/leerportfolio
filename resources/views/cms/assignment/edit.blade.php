@extends('layouts.cms')

@section('title')Edit assignment @endsection

@section('content')
    <div class="container">
        <div class="row">
            {!! Form::open(array('route' => 'update_assignment','files'=>true)) !!}
                <input type="hidden" name="id" value="{{$assignment->id}}">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Beeldassosiaties" value="{{$assignment->title}}" required>
                </div>
                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" class="form-control" id="slug" name="slug" placeholder="beeldassosiaties" value="{{$assignment->slug}}" required>
                </div>
                <div class="form-group">
                    <label for="introduction">Introduction</label><br>
                    <textarea class="form-control" id="introduction" name="introduction" required>{{$assignment->introduction}}</textarea>
                </div>
                <div class="form-group">
                    <label for="assignment_image">Image</label>
                    <input type="file" class="form-control" id="assignment_image" name="assignment_image">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Create</button>
                </div>
            {!! Form::close() !!}

        </div>
    </div>

    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection
