@extends('layouts.cms')

@section('title')Create assignment @endsection

@section('content')
    <div class="container">
        <div class="row">
            {!! Form::open(array('route' => 'create_assignment','files'=>true)) !!}
                <input type="hidden" name="course_id" value="{{$course_id}}">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Beeldassosiaties" required>
                </div>
                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" class="form-control" id="slug" name="slug" placeholder="beeldassosiaties" required>
                </div>
                <div class="form-group">
                    <label for="introduction">Introduction</label><br>
                    <textarea class="form-control" id="introduction" name="introduction" required></textarea>
                </div>
                <div class="form-group">
                    <label for="assignment_image">Image</label>
                    <input type="file" class="form-control" id="assignment_image" name="assignment_image">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Create</button>
                </div>

            {!! Form::close() !!}

        </div>
    </div>

        <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
        <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
        <script>
            $('textarea').ckeditor();
        </script>
@endsection
