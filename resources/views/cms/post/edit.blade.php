@extends('layouts.cms')

@section('title')Edit post @endsection

@section('content')
    <div class="container">
        <div class="row">
            {!! Form::open(array('route' => 'update_post','files'=>true)) !!}
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="form-group">
                    <label for="type">Type</label>
                    <select name="type" class="form-control">
                            <option value="1" @if ($post->type == '1') selected @endif>Full image</option>
                            <option value="2" @if ($post->type == '2') selected @endif>Full text</option>
                            <option value="3" @if ($post->type == '3') selected @endif>50/50 - text, images</option>
                            <option value="4" @if ($post->type == '4') selected @endif>50/50 - images, text</option>
                            <option value="5" @if ($post->type == '5') selected @endif>30/70 - text, images</option>
                            <option value="6" @if ($post->type == '6') selected @endif>Download</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Beeldassosiaties" value="{{$post->title}}">
                </div>
                <div class="form-group">
                    <label for="content">Content</label><br>
                    <textarea class="form-control" id="content" name="content">{{$post->content}}</textarea>
                </div>
                <div class="form-group">
                    <label for="post_image">Images</label>
                    <input type="file" class="form-control" id="post_image" name="post_image[]" multiple>
                </div>
                <div class="form-group">
                    <label for="post_file">File</label>
                    <input type="file" class="form-control" id="post_file" name="post_file">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Update</button>
                </div>

            {!! Form::close() !!}

        </div>
    </div>

    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection
