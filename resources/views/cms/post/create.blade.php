@extends('layouts.cms')

@section('title')Create post @endsection

@section('content')
    <div class="container">
        <div class="row">
            {!! Form::open(array('route' => 'create_post','files'=>true)) !!}
                <input type="hidden" name="assignment_id" value="{{$assignment_id}}">
                <div class="form-group">
                    <label for="type">Type</label>
                    <select name="type" class="form-control">
                            <option value="1">Full image</option>
                            <option value="2" selected>Full text</option>
                            <option value="3">50/50 - text, images</option>
                            <option value="4">50/50 - images, text</option>
                            <option value="5">30/70 - text, images</option>
                            <option value="6">Download</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Beeldassosiaties">
                </div>
                <div class="form-group">
                    <label for="content">Content</label><br>
                    <textarea class="form-control" id="content" name="content"></textarea>
                </div>
                <div class="form-group">
                    <label for="post_image">Image</label>
                    <input type="file" class="form-control" id="post_image" name="post_image[]" multiple>
                </div>
                <div class="form-group">
                    <label for="post_file">File</label>
                    <input type="file" class="form-control" id="post_file" name="post_file">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Create</button>
                </div>

            {!! Form::close() !!}

        </div>
    </div>

        <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
        <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
        <script>
            $('textarea').ckeditor();
        </script>
@endsection
