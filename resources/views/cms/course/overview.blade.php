@extends('layouts.cms')

@section('title')Course overview @endsection

@section('content')
    <div class="container">
        <div class="row">
            @foreach ($courses as $course)
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $course->title }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $course->abbreviation }}</h6>
                            <a href='{{url("/assignments/$course->id")}}' class="card-link btn btn-light">View</a>
                            <a href='{{url("/edit_course/$course->id")}}' class="card-link btn btn-primary">Edit</a>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" data-id="{{$course->id}}" data-title="{{$course->title}}">Delete</button>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <a href="{{url('/create_course')}}" class="btn btn-primary">Create</a>
    </div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          Are you sure you want to delete this course? This can not be undone.
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var title = button.data('title')
            var modal = $(this)
            modal.find('.modal-title').text('Are you sure you want to delete ' + title)
            modal.find('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><a href="{{url("/")}}/delete_course/'+id+'" class="card-link btn btn-danger">Delete</a>')
        })
    });

</script>
@endsection
