@extends('layouts.cms')

@section('title')Edit course @endsection

@section('content')
    <div class="container">
        <div class="row">
            {!! Form::open(array('route' => 'update_course','files'=>true)) !!}
                <input type="hidden" name="id" value="{{$course->id}}">
                <div class="form-group">
                    <label for="abbreviation">Abbreviation</label>
                    <input type="text" class="form-control" id="abbreviation" name="abbreviation" placeholder="SCO" value="{{$course->abbreviation}}">
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Strategie & Concepting" value="{{$course->title}}">
                </div>
                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" class="form-control" id="slug" name="slug" placeholder="sco" value="{{$course->slug}}">
                </div>
                <div class="form-group">
                    <label for="course_image">Image</label>
                    <input type="file" class="form-control" id="course_image" name="course_image" placeholder="sco">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Change</button>
                </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
