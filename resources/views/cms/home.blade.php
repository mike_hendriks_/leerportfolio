@extends('layouts.cms')

@section('content')
    <div class="container">
        <div class="row">

        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Courses</h5>
                    <p class="card-text">Create, update and delete courses.</p>
                    <a href="{{url('/courses')}}" class="card-link">Edit</a>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Assignments</h5>
                    <p class="card-text">Create, update and delete assignments.</p>
                    <a href="{{url('/assignments')}}" class="card-link">Edit</a>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Posts</h5>
                    <p class="card-text">Create, update and delete posts.</p>
                    <a href="{{url('/posts')}}" class="card-link">Edit</a>
                </div>
            </div>
        </div>

        </div>
    </div>
@endsection
