@extends('layouts.app')

@section('content')
    <header>
		<div class="tv">
			<div class="screen mute" id="tv"></div>
		</div>

		<div class="bg_overlay"></div>
        <main class="no_effect">
    		<div id="txt">
    			<h1 class="glitch top" data-text="GLITCH" id="mh">Mike Hendriks</h1>
    			<h1 class="glitch bottom" data-text="GLITCH" id="mh">Mike Hendriks</h1>
    			<h2 class="glitch top">Full stack web developer</h2>
    			<h2 class="glitch bottom">Full stack web developer</h2>
    		</div>
        </main>
	</header>
@endsection
