@extends('layouts.app')

@section('content')
<main class="course">
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <h1>{{ $course->title }}</h1>
            </div>
        </div>
        <div class="row">
            @if (count($assignments) >= 1)
                @foreach ($assignments as $i => $assignment)
                    <div class="col m4 assignment offset-m1 rellax has_overlay @if ($i & 1) odd @endif @if ($i < 4)fade_on_load @endif" data-rellax-speed="{{ rand(0, 1) }}" style="margin-right: {{ rand(-60, 60) }}px;">
                        <div class="overlay_container">
                            <div class="block_overlay"></div>
                        </div>
                        <a href="/assignment/{{ $assignment->slug }}">
                            <div class="image_container">
                                <img src="{{asset('images/'.$assignment->image)}}" alt="" class="img-responsive">
                            </div>
                            <div class="content_container">
                                <h2>{{ $assignment->title }}</h2>
                                <p>{!! $assignment->introduction !!}</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            @else
                <h5 class="center-align">There are no assignments for this course</h5>
            @endif

        </div>
        @if (Auth::check())
            <div class="fixed-action-btn">
                <a href="/create_assignment/{{$course->id}}" class="btn-floating blue lighten-2">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif
    </div>

</main>
@endsection
