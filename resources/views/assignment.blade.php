@extends('layouts.app')

@section('content')
<main class="post">
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <h1>{{ $assignment->title }}</h1>
            </div>
        </div>
    </div>

    @foreach ($posts as $post)
        <div class="post">
            @if ($post['type'] == '1')
                @include('post_types.full_image')
            @elseif ($post['type'] == '2')
                @include('post_types.full_text')
            @elseif ($post['type'] == '3')
                @include('post_types.text_img')
            @elseif ($post['type'] == '4')
                @include('post_types.img_text')
            @elseif ($post['type'] == '5')
                @include('post_types.30_70_text_image')
            @elseif ($post['type'] == '6')
                @include('post_types.download')
            @endif
            @if (Auth::check())
                <a href="/edit_post/{{$post->id}}" class="edit_post btn-floating waves-effect waves-light blue lighten-2"><i class="material-icons">edit</i></a>
            @endif
        </div>
    @endforeach
    <div class="container">
        <div class="pagination">
            @if (!empty($previous))
                <a href="/assignment/{{$previous->slug}}" class="left button">Previous assignment</a>
            @endif
            @if (!empty($next))
                <a href="/assignment/{{$next->slug}}" class="right button">Next assignment</a>
            @endif
        </div>
    </div>
    @if (Auth::check())
        <div class="fixed-action-btn">
            <a href="/create_post/{{$assignment->id}}" class="btn-floating blue lighten-2">
                <i class="large material-icons">add</i>
            </a>
        </div>
    @endif
</main>
@endsection
