<div class="container">
    <div class="row">
        <div class="col s12">
            @if (isset($post->file))
                <a href="{{url('uploads/'.$post->file)}}" class="button" target="_blank">Bekijk {{ $post->title }}</a>
            @endif
        </div>
    </div>
</div>
