<div class="container">
    <div class="row">
        <div class="col m6">
            <h3>{{ $post->title }}</h3>
            {!! $post->content !!}
        </div>
        <div class="col m6">
            <div class="carousel owl-carousel">
                @php
            		$images = unserialize($post['images']);
            	@endphp
            	@if (count($images) > 1)
            		<div class="carousel owl-carousel">
            	@endif
                @if (count($images) > 0)
                    @foreach ($images as $image)
                        <img src="{{asset('images/'.$image)}}" alt="" class="img-responsive">
                    @endforeach
                @endif
            	@if (count($images) > 1)
            		</div>
            	@endif
            </div>
        </div>
    </div>
</div>
